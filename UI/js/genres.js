$.ajax({ url: "https://localhost:44310/Genres", type: "GET", success: genreSuccessFunction, error: genreErrorFunction });

function genreSuccessFunction(response) {
    $('.container-fluid').empty();
    response.forEach(genre => {
        let child = `
        <div class="container">
            <div class="card text-center">
                <div class="card-header">${genre.name}
                    <button class="deleteButton"  onclick=deleteGenre(${genre.id})>
                        <i class="material-icons">delete</i>
                    </button>
                    <button class="editButton" onclick=editGenre(${genre.id}) data-toggle="modal" data-target="#genreModal">
                        <i class="material-icons">edit</i>
                    </button> 
                </div>
            </div>
        </div>`;
        $(".container-fluid").append(child);
    });
}

function genreErrorFunction(error) {
    console.log(error);
}

function addGenre() {
    var a = JSON.stringify({
        "Name": $("#genreName").val()
    });
    console.log(a);
    $.ajax({
        url: "https://localhost:44310/genres",
        type: "POST",
        contentType: "application / json;charset = utf - 8",
        data: a,
        success: function() {
            window.alert("Genre got added successfully.");
            $("#genreClose").trigger("click");
        },
        error: function(response) {
            console.log(response.responseText);
        },
        async: false
    });
    $.ajax({ url: "https://localhost:44310/genres", type: "GET", success: genreSuccessFunction, error: genreErrorFunction });
}
const deleteGenre = (id) => {
    console.log("DELETE");
    console.log(id);
    const url = "https://localhost:44310/genres/" + id
    $.ajax({
        url: url,
        type: "DELETE",
        success: function(data) {
            console.info(data);
            window.alert("Genre got succesfully deleted.");
            $.ajax({ url: "https://localhost:44310/genres", type: "GET", success: genreSuccessFunction, error: genreErrorFunction });
        },
        error: function(err) {
            console.log(err)
            window.alert(err.responseText)
        }
    });
}
const editGenre = (id) => {
    $.get("https://localhost:44310/Genres/" + id, function(response) {
        console.log("EDIT", response);
        const GenreForm = `
        Name
        <div class="form-group">
            <input type="text" class="form-control" id="GenreName" value=${response.name} name='newGenreName'>
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
        <button type="button" id="genreCloseModal" class="btn btn-secondary" data-dismiss="modal">Close</button>
     `;
        $("#editGenreModalBody").html(GenreForm);
        $("#editGenreModalBody").submit((e) => {
            e.preventDefault();
            const name = e.target.newGenreName.value
            const request = {
                id: response.id,
                name: name === "" ? response.name : name !== response.name ? name : response.name
            };
            var o = JSON.stringify(request);
            $.ajax({
                url: "https://localhost:44310/Genres/" + id,
                type: "PUT",
                contentType: "application/json; charset=utf-8",
                data: o,
                success: function() {
                    $("#genreCloseModal").trigger("click");
                    window.alert("success");
                },
                error: function(error) {
                    console.log(error);
                },
                async: false
            });
            location.reload();
            $.ajax({ url: "https://localhost:44310/Genres", type: "GET", success: genreSuccessFunction, error: genreErrorFunction });
        });
    });
}
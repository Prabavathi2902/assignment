$.ajax({ url: "https://localhost:44310/Producers", type: "GET", success: producerSuccessFunction, error: producerErrorFunction });

function producerSuccessFunction(response) {
    $('.container-fluid').empty();
    response.forEach(producer => {
        let child =
            ` <div class="container">
                 <div class="card text-center">
                    <div class="card-header">${producer.name}
                        <button class="deleteButton" onclick=deleteProducer(${producer.id})>
                            <i class="material-icons">delete</i>
                        </button>
                        <button class="editButton" onclick=editProducer(${producer.id}) data-toggle="modal" data-target="#producerModal">
                            <i class="material-icons">edit</i>
                        </button> 
                    </div>
                    <div class="card-body">
                        <p class="card-text">${producer.biography}</p>
                    </div>
                    <div class="card-footer text-muted">
                        <p>${producer.dob} ${producer.gender}</p>
                    </div>
                </div>
            </div>
        <br>`;
        $(".container-fluid").append(child);
    });
}

function producerErrorFunction(error) {
    console.log(error);
}

function addProducer() {
    var a = JSON.stringify({
        "Name": $("#producerName").val(),
        "Gender": $("#producerGender ").val(),
        "DOB": $("#producerDOB").val(),
        "Biography": $("#producerBio").val()
    });
    console.log(a);
    $.ajax({
        url: "https://localhost:44310/producers",
        type: "POST",
        contentType: "application / json;charset = utf - 8",
        data: a,
        success: function() {
            window.alert("Producer got added successfully.");
            $("#producerClose").trigger("click");
        },
        error: function(error) {
            console.log(error);
        },
    });
    $.ajax({ url: "https://localhost:44310/producers", type: "GET", success: producerSuccessFunction, error: producerErrorFunction });
}
const deleteProducer = (id) => {
    console.log("DELETE");
    console.log(id);
    const url = "https://localhost:44310/producers/" + id
    $.ajax({
        url: url,
        type: "DELETE",
        success: function(data) {
            console.info(data);
            window.alert("Producer got succesfully deleted.");
            $.ajax({ url: "https://localhost:44310/producers", type: "GET", success: producerSuccessFunction, error: producerErrorFunction });
        },
        error: function(err) {
            console.log(err);
            window.alert(err.responseText);
        }
    });
}
const editProducer = (id) => {
    $.get("https://localhost:44310/Producers/" + id, function(response) {
        console.log("EDIT", response);
        const ProducerForm = `
        <div class="form-group">
        Name : 
            <input type="text" class="form-control" id="ProducerName" value="${response.name}" name='newProducerName'>
        </div>
        <div class="form-group">
        DOB : 
            <input type="date" class="form-control" id="producerDOB" max="2021-05-11" value=${new Date(response.dob).getFullYear()}-${('0' + (new Date(response.dob).getMonth() + 1)).slice(-2)}-${('0' + new Date(response.dob).getDate()).slice(-2)} name='newProducerDob'>
        </div>
        <div class="form-group">
        Biography :
            <textarea class="form-control" id="producerBio" name='newProducerBio' rows="3">${response.biography}</textarea>
        </div>
        <div class="form-group">
        Gender :
            <select class="form-control" id="ProducerGender" name='newProducerGender'>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
        <button type="button" id="producerCloseModal" class="btn btn-secondary" data-dismiss="modal">Close</button> `;
        $("#editProducerModalBody").html(ProducerForm);
        $("#newProducerGender").val(response.gender);
        $("#editProducerModalBody").submit((e) => {
            e.preventDefault();
            const bio = e.target.newProducerBio.value
            const dob = e.target.newProducerDob.value
            const sex = e.target.newProducerGender.value
            const name = e.target.newProducerName.value
            const request = {
                id: response.id,
                name: name === "" ? response.name : name !== response.name ? name : response.name,
                biography: bio === "" ? response.biography : bio !== response.biography ? bio : response.biography,
                gender: sex === "" ? response.gender : sex !== response.gender ? sex : response.gender,
                dob: dob === "" ? response.dob : dob !== response.dob ? dob : response.dob,
            };
            var o = JSON.stringify(request);
            console.log(o);
            $.ajax({
                url: "https://localhost:44310/Producers/" + id,
                type: "PUT",
                contentType: "application/json; charset=utf-8",
                data: o,
                success: function() {
                    $("#producerCloseModal").trigger("click");
                    window.alert("Producer got successfully Updated");
                },
                error: function(error) {
                    console.log(error);
                },
                async: false
            });
            location.reload();
            $.ajax({ url: "https://localhost:44310/Producers", type: "GET", success: producerSuccessFunction, error: producerErrorFunction });
        });
    });
}
$.ajax({ url: "https://localhost:44310/Actors", type: "GET", success: actorSuccessFunction, error: actorErrorFunction });

function actorSuccessFunction(response) {
    $('.container-fluid').empty();
    response.forEach(actor => {
        let child =
            `<div class="container">
                <div class="card text-center">
                    <div class="card-header">${actor.name}
                        <button class="deleteButton" onclick=deleteActor(${actor.id})">
                            <i class="material-icons">delete</i>
                        </button>
                        <button class="editButton" onclick=editActor(${actor.id}) data-toggle="modal" data-target="#actorModal">
                            <i class="material-icons">edit</i>
                        </button> 
                    </div>
                    <div class="card-body">
                        <p class="card-text">${actor.biography}</p>
                    </div>
                    <div class="card-footer text-muted">
                         <p>${actor.dob} ${actor.gender}</p>
                    </div>
                </div>
            </div>
        <br>`;
        $(".container-fluid").append(child);
    });
}

function actorErrorFunction(error) {
    console.log(error);
}

function addActor() {
    var a = JSON.stringify({
        "Name": $("#actorName").val(),
        "Gender": $("#actorGender ").val(),
        "DOB": $("#actorDOB").val(),
        "Biography": $("#actorBio").val()
    });
    console.log(a);
    $.ajax({
        url: "https://localhost:44310/actors",
        type: "POST",
        contentType: "application / json;charset = utf - 8",
        data: a,
        success: function() {
            window.alert("Actor got successfully added");
            $("#actorClose").trigger("click");
        },
        error: function(error) {
            console.log(error);
        },
        async: false
    });
    $.ajax({ url: "https://localhost:44310/actors", type: "GET", success: actorSuccessFunction, error: actorErrorFunction });
}
const deleteActor = (id) => {
    console.log("DELETE");
    console.log(id);
    const url = "https://localhost:44310/actors/" + id
    $.ajax({
        url: url,
        type: "DELETE",
        success: function(data) {
            window.alert("Actor got succesfully deleted.");
            $.ajax({ url: "https://localhost:44310/actors", type: "GET", success: actorSuccessFunction, error: actorErrorFunction });
        },
        error: function(err) {
            console.log(err)
            window.alert(err.responseText)
        }
    });
}
const editActor = (id) => {
    $.get("https://localhost:44310/Actors/" + id, function(response) {
        const ActorForm = `
        <div class="form-group">
        Name : 
            <input type="text" class="form-control" id="ActorName" value="${response.name}" name='newActorName'>
        </div>
        <div class="form-group">
        DOB :  
            <input type="date" class="form-control" id="actorDOB" max="2021-05-11" value=${new Date(response.dob).getFullYear()}-${('0' + (new Date(response.dob).getMonth() + 1)).slice(-2)}-${('0' + new Date(response.dob).getDate()).slice(-2)}  name='newActorDob'>
        </div>
        <div class="form-group">
        Biography : 
              <textarea class="form-control" name='newActorBio' rows="3">${response.biography}</textarea>
        </div>
        <div class="form-group">
        Gender :
        <select class="form-control" id="newActorGender" name="sex">
            <option>Male</option>
            <option>Female</option>
        </select>
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
        <button type="button" id="actorCloseModal" class="btn btn-secondary" data-dismiss="modal">Close</button>`;
        $("#editActorModalBody").html(ActorForm);
        $("#newActorGender").val(response.gender);
        $("#editActorModalBody").submit((e) => {
            e.preventDefault();
            const bio = e.target.newActorBio.value
            const dob = e.target.newActorDob.value
            const sex = e.target.sex.value
            const name = e.target.newActorName.value
            const request = {
                id: response.id,
                name: name === "" ? response.name : name !== response.name ? name : response.name,
                biography: bio === "" ? response.biography : bio !== response.biography ? bio : response.biography,
                gender: sex === "" ? response.gender : sex !== response.gender ? sex : response.gender,
                dob: dob === "" ? response.dob : dob !== response.dob ? dob : response.dob,
            };
            var o = JSON.stringify(request);
            $.ajax({
                url: "https://localhost:44310/Actors/" + id,
                type: "PUT",
                contentType: "application/json; charset=utf-8",
                data: o,
                success: function() {
                    $("#actorCloseModal").trigger("click");
                    window.alert("Actor got successfully Updated");
                },
                error: function(error) {
                    console.log(error);
                },
                async: false
            });
            location.reload();
            $.ajax({ url: "https://localhost:44310/Actors", type: "GET", success: actorSuccessFunction, error: actorErrorFunction });
        });
    });
}
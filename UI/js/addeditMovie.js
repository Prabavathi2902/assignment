$(document).ready(function() {
    $("#genres").select2();
    $('#actors').select2();
    $.ajax({ url: "https://localhost:44310/producers", type: "GET", success: producerSuccess, error: producerError });
    $.ajax({ url: "https://localhost:44310/actors", type: "GET", success: actorSuccess, error: actorError });
    $.ajax({ url: "https://localhost:44310/genres", type: "GET", success: genreSuccess, error: genreError });
    let query = window.location.search;
    let urlParams = new URLSearchParams(query);
    let id = urlParams.get('id');
    if (id == null) {
        $("#movieSubmit").attr("onclick", `addMovie()`);
    } else {
        $.ajax({ url: "https://localhost:44310/Movies/" + id, type: "GET", success: movieSuccess, error: movieError });
    }
});
var producers = [];
var actors = [];
var genres = [];

function producerSuccess(response) {
    producers = response;
    populateProducersList();
}

function producerError(error) {
    console.log(error);
}

function populateProducersList() {
    let childs = [`<option value="">Producer</option>`];
    producers.forEach(producer => {
        childs.push(`<option value="${producer.id}">${producer.name}</option>`);
    });
    $("#producer").html(childs.join(''));
}

function actorSuccess(response) {
    actors = response;
    populateActorsList();
}

function actorError(error) {
    console.log(error);
}

function populateActorsList() {
    let childs = [];
    actors.forEach(actor => {
        childs.push(`<option value="${actor.id}">${actor.name}</option>`);
    });
    $("#actors").html(childs.join(" "));
}

function genreSuccess(response) {
    genres = response;
    populateGenresList();
}

function genreError(error) {
    console.log(error);
}

function populateGenresList() {
    let childs = [];
    genres.forEach(genre => {
        childs.push(`<option value="${genre.id}">${genre.name}</option>`)
    });
    $("#genres").html(childs.join(" "));
}

function addActor() {
    let postData = JSON.stringify({
        "name": $("#actorName").val(),
        "biography": $("#actorBio").val(),
        "dob": $("#actorDOB").val(),
        "gender": $("#actorGender").val()
    });
    $.ajax({
        url: "https://localhost:44310/actors",
        type: "POST",
        data: postData,
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            $.ajax({
                url: "https://localhost:44310/actors/" + response.id,
                type: "GET",
                success: function(response) {
                    actors.push(response);
                    $("#addActorClose").trigger("click");
                    $("#actorName").val("");
                    $("#actorBio").val("");
                    $("#actorDOB").val("");
                    $("actorGender").val("_");
                    populateActorsList();
                    alert("Adding actor successfully completed.");
                    $.ajax({ url: "https://localhost:44310/actors", type: "GET", success: actorSuccess, error: actorError });
                },
                error: function(error) {
                    console.log(error);
                }
            });
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function addProducer() {
    let postData = JSON.stringify({
        "name": $("#producerName").val(),
        "biography": $("#producerBio").val(),
        "dob": $("#producerDOB").val(),
        "gender": $("#producerGender").val()
    });
    $.ajax({
        url: "https://localhost:44310/producers",
        type: "POST",
        data: postData,
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            $.ajax({
                url: "https://localhost:44310/producers/" + response.id,
                type: "GET",
                success: function(response) {
                    producers.push(response);
                    $("#producerClose").trigger("click");
                    $("#producerName").val("");
                    $("#producerBio").val("");
                    $("#producerDOB").val("");
                    $("#producerGender").val("_");
                    populateProducersList();
                    alert("Adding producer successfully completed.");
                    $(`select[id="producerSelect"] option[value="${response.id}"]`).attr("selected", "selected");
                },
                error: function(error) {
                    console.log(error);
                }
            });
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function addGenre() {
    let postData = JSON.stringify({
        "name": $("#genreName").val()
    });
    $.ajax({
        url: "https://localhost:44310/genres",
        type: "POST",
        data: postData,
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            $.ajax({
                url: "https://localhost:44310/genres/" + response.id,
                type: "GET",
                success: function(response) {
                    genres.push(response);
                    $("#addGenreClose").trigger("click");
                    $("#genreName").val("");
                    populateGenresList();
                    alert("Adding genre succesfully completed.");
                    $.ajax({ url: "https://localhost:44310/genres", type: "GET", success: genreSuccess, error: genreError });
                },
                error: function(error) {
                    console.log(error);
                }
            });
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function movieSuccess(response) {
    console.log(response);
    $("#movieName").val(response.name);
    $("#yearOfRelease").val(response.yearOfRelease);
    $("#producer").val(response.producer.id);
    response.actors.forEach(actor => {
        $(`select[id="actors"] option[value="${actor.id}"]`).attr("selected", "selected");
        $("#actors").trigger("change");
    });
    response.genres.forEach(genre => {
        $(`select[id="genres"] option[value="${genre.id}"]`).attr("selected", "selected");
        $("#genres").trigger("change");
    });
    $("#plot").val(response.plot);
    $("#movieSubmit").attr("onclick", `editMovie(${response.id})`);
}

function movieError(error) {
    console.log(error);
}

function editMovie(id) {
    let putData = JSON.stringify({
        "name": $("#movieName").val(),
        "yearOfRelease": $("#yearOfRelease").val(),
        "producerId": $("#producer").val(),
        "actorIds": $("#actors").val(),
        "genreIds": $("#genres").val(),
        "plot": $("#plot").val(),
        "poster": $("#poster").val()
    });
    $.ajax({
        url: "https://localhost:44310/movies/" + id,
        type: "PUT",
        data: putData,
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            alert("Movie updated successfully.");
            location.href = "movies.html";
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function addMovie() {
    var a = JSON.stringify({
        "Name": $("#movieName").val(),
        "YearOfRelease": $("#yearOfRelease").val(),
        "ProducerId": $("#producer").val(),
        "ActorIds": $("#actors").val(),
        "GenreIds": $("#genres").val(),
        "Plot": $("#plot").val(),
        "Poster": $("#poster").val()
    });
    console.log(a);
    $.ajax({
        url: "https://localhost:44310/Movies",
        type: "POST",
        contentType: "application / json;charset = utf - 8",
        data: a,
        success: function() {
            window.alert("Movie got added successfully.");
            location.href = "movies.html";
        },
        error: function(error) {
            console.log(error);
        },
        async: false
    });
}
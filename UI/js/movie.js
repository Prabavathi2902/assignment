$.ajax({ url: "https://localhost:44310/Movies", type: "GET", success: movieSuccessFunction, error: movieErrorFunction });

function movieSuccessFunction(response) {
    $('.row').empty();
    response.forEach(movie => {
        let movieGenres = [];
        movie.genres.forEach(genre => {
            movieGenres.push(genre.name);
        });
        let movieActors = [];
        movie.actors.forEach(actor => {
            movieActors.push(actor.name);
        });
        let child =
            ` 
            <div class="col-md-6">
                <div class="card border-dark mb-3">
                    <div class="card-horizontal">
                        <div class="image">
                            <img class style="width: 200px;height:200px;margin: 10px;margin-top: 15px; "src="images/1.jpg">
                        </div>
                        <div class="card-body" style="width: 200px;height:350px;margin: 10px;">
                            <h5 class="card-title" style="color: rgb(231, 127, 96);font-size: larger;">${movie.name}</h5>
                            <h4 class="card-title">${movie.yearOfRelease}</h4>
                            <hr>
                            <span class="genres">${movieGenres.join(", ")}</span>
                            <p class="card-text">${movie.plot}</p>
                            <span class="producer">Producer: ${movie.producer.name}</span>
                        </div>
                        <div class="actionButtons" style="width: 200px;height:350px;margin: 10px;">
                            <button class="deleteButton" onclick=deleteMovie(${movie.id})><span class="material-icons">delete</span></button>
                            <button class="editButton" onclick="location.href='http://127.0.0.1:5500/addeditMovie.html'+'?id=${movie.id}'"><span class="material-icons">edit</span></button>
                        </div>
                    </div>
                    <div class="card-footer">
                        <span class="actors">Cast: ${movieActors.join(", ")}</span><br>
                    </div>
                </div>
            </div>`;
        $(".row").append(child);
    });
}

function movieErrorFunction(error) {
    console.log(error);
}
const deleteMovie = (id) => {
    console.log("DELETE");
    console.log(id);
    const url = "https://localhost:44310/movies/" + id
    $.ajax({
        url: url,
        type: "DELETE",
        success: function(data) {
            console.info(data);
            window.alert("Movie got succesfully deleted.");
            $.ajax({ url: "https://localhost:44310/movies", type: "GET", success: movieSuccessFunction, error: movieErrorFunction });
        },
        error: function(err) {
            console.log(err)
            window.alert(err.responseText)
        }
    });
}
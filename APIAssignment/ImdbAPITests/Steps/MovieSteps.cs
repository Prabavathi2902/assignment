﻿using ImdbAPI;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using ImdbAPITests.MockResources;
namespace ImdbAPITests.Steps
{
    [Scope(Feature = "Movie Resource")]
    [Binding]
    public class MovieSteps : BaseSteps
    {
        public MovieSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => MovieMock.movieRepoMock.Object);
                    services.AddScoped(service => ActorMock.actorRepoMock.Object);
                    services.AddScoped(service => ProducerMock.producerRepoMock.Object);
                    services.AddScoped(service => GenreMock.genreRepoMock.Object);
                });
            }))
        { }
        [BeforeScenario]
        public void MockRepositories()
        {
            MovieMock.MockGet();
            MovieMock.MockGetByQuery();
            MovieMock.MockGetById();
            MovieMock.MockAdd();
            MovieMock.MockUpdate();
            MovieMock.MockDelete();
            ProducerMock.MockGetById();
            ActorMock.MockGetByMovieId();
            ActorMock.MockGetById();
            GenreMock.MockGetById();
            GenreMock.MockGetByMovieId();
        }
    }
}
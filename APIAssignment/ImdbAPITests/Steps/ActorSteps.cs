﻿using ImdbAPI;
using ImdbAPITests.MockResources;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
namespace ImdbAPITests
{
    [Scope(Feature="Actor Resource")]
    [Binding]
    public class ActorSteps : BaseSteps
    {
        public ActorSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
            builder.ConfigureServices(services =>
            {
                services.AddScoped(service => ActorMock.actorRepoMock.Object);
            });
        }))
        { }
        [BeforeScenario]
        public void MockRepositories()
        {
            ActorMock.MockGet();
            ActorMock.MockGetById();
            ActorMock.MockAdd();
            ActorMock.MockUpdate();
            ActorMock.MockDelete();
            ActorMock.MockPatch();
        }    
    }
}
﻿using ImdbAPI;
using ImdbAPITests.MockResources;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
namespace ImdbAPITests.Steps
{
    [Scope(Feature = "Producer Resource")]
    [Binding]
    public class ProducerSteps : BaseSteps
    {
        public ProducerSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => ProducerMock.producerRepoMock.Object);
                });
            }))
        { }
        [BeforeScenario]
        public void MockRepositories()
        {
            ProducerMock.MockGet();
            ProducerMock.MockGetById();
            ProducerMock.MockAdd();
            ProducerMock.MockUpdate();
            ProducerMock.MockDelete();
        }
    }
}
﻿using ImdbAPI;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using Xunit;
namespace ImdbAPITests
{
    public class BaseSteps
    {
        protected HttpResponseMessage _httpResponseMessage { get; set; }
        protected HttpClient Client { get; set; }
        protected WebApplicationFactory<TestStartup> Factory { get; set; }
        public BaseSteps(WebApplicationFactory<TestStartup> baseFactory)
        {
            Factory = baseFactory;
        }
        [Given(@"I am a client")]
        public void GivenIAmAClient()
        {
            Client = Factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                BaseAddress=new Uri("http://localhost")
            });
        }
        [When(@"I make a GET Request '(.*)' with following data '(.*)'")]
        public async Task WhenIMakeAGETRequestWithFollowingData(string endPoint, string data)
        {
            var getRelativeUri = new Uri(endPoint + $"?{data}", UriKind.Relative);
            _httpResponseMessage = await Client.GetAsync(getRelativeUri);
        }

        [When(@"I make GET Request '(.*)'")]
        public async Task WhenIMakeGETRequestWithTheFollowingData(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            _httpResponseMessage = await Client.GetAsync(uri);
        }
        [Then(@"response code must be '(.*)'")]
        public void ThenResponseCodeMustBe(int statusCode)
        {
            var expectedStatusCode = (HttpStatusCode)statusCode;
            Assert.Equal(expectedStatusCode, _httpResponseMessage.StatusCode);
        }
        [Then(@"response data must look like '(.*)'")]
        public async Task ThenResponseDataMustLookLike(string response)
        {
            var responseData = await _httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            Assert.Equal(response, responseData);
        }
        [When(@"I making a POST request to '(.*)' with the following data '(.*)'")]
        public async Task WhenIMakingAPOSTRequestToWithTheFollowingData(string resourceEndPoint, string postDataJson)
        {
            var postRelativeUri = new Uri(resourceEndPoint, UriKind.Relative);
            var content = new StringContent(postDataJson, Encoding.UTF8, "application/json");
            _httpResponseMessage = await Client.PostAsync(postRelativeUri, content);
        }
        [When(@"I make a Patch Request to '(.*)' with the following Data '(.*)'")]
        public async Task WhenIMakeAPatchRequestToWithTheFollowingData(string resourceEndPoint, string postDataJson)
        {
            var postRelativeUri = new Uri(resourceEndPoint, UriKind.Relative);
            var content = new StringContent(postDataJson, Encoding.UTF8, "application/json");
            _httpResponseMessage = await Client.PatchAsync(postRelativeUri, content);
        }

        [When(@"I make a PUT Request to '(.*)' with the following Data '(.*)'")]
        public async Task WhenIMakeAPUTRequestToWithTheFollowingData(string resourceEndPoint, string postDataJson)
        {
            var postRelativeUri = new Uri(resourceEndPoint, UriKind.Relative);
            var content = new StringContent(postDataJson, Encoding.UTF8, "application/json");
            _httpResponseMessage = await Client.PutAsync(postRelativeUri, content);
        }
        [When(@"I make Delete Request to '(.*)'")]
        public async Task WhenIMakeDeleteRequestTo(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            _httpResponseMessage = await Client.DeleteAsync(uri);
        }
    }
}
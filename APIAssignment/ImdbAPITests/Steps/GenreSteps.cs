﻿using ImdbAPI;
using ImdbAPITests.MockResources;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
namespace ImdbAPITests.Steps
{
    [Scope(Feature = "Genre Resource")]
    [Binding]
    public class GenreSteps : BaseSteps
    {
        public GenreSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => GenreMock.genreRepoMock.Object);
                });
            }))
        { }
        [BeforeScenario]
        public void MockRepositories()
        {
            GenreMock.MockGet();
            GenreMock.MockGetById();
            GenreMock.MockAdd();
            GenreMock.MockUpdate();
            GenreMock.MockDelete();         
        }
    }
}
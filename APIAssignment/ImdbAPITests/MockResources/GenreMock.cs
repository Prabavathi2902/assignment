﻿using ImdbAPI.Models.DB;
using ImdbAPI.Repository;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace ImdbAPITests.MockResources
{
    public class GenreMock
    {
        public static readonly Mock<IGenreRepository> genreRepoMock = new Mock<IGenreRepository>();
        public static void MockGet()
        {
            genreRepoMock.Setup(repo => repo.Get()).Returns(() =>
            {
                return ListOfGenres();
            });
        }
        private static IEnumerable<Genre> ListOfGenres()
        {
            var list = new List<Genre>()
            {
                new Genre()
                {
                 Id=1,
                 Name="Action"
                },
                new Genre()
                {
                 Id=2,
                 Name="Comedy"
                }
            };
            return list;
        }
        public static void MockGetById()
        {
            genreRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) =>
            {
                return ListOfGenres().FirstOrDefault(a => a.Id == id);
            }
            );
        }
        public static void MockAdd()
        {
            genreRepoMock.Setup(repo => repo.Add(It.IsAny<Genre>())).Returns(1);
        }
        public static void MockUpdate()
        {
            genreRepoMock.Setup(repo => repo.Update(It.IsAny<Genre>()));
        }
        public static void MockDelete()
        {
            genreRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
        public static void MockGetByMovieId()
        {
            genreRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>())).Returns((int movieId) =>
            {
                return ListOfGenres();
            });
        }
    }
}
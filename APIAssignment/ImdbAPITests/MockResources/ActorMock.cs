﻿using ImdbAPI.Models.DB;
using ImdbAPI.Repository;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImdbAPITests.MockResources
{
    public class ActorMock
    {
        public static readonly Mock<IActorRepository> actorRepoMock = new Mock<IActorRepository>();
        public static void MockGet()
        {
            actorRepoMock.Setup(repo => repo.Get()).Returns(() =>
              {
                  return ListOfActors();
              });
        }
        private static IEnumerable<Actor> ListOfActors()
        {
            var list = new List<Actor>()
            {
                new Actor
                {
                 Id=1,
                 Name="Christian Bale",
                 Biography="British",
                 DOB=new DateTime(1979,03,02),
                 Gender="Male"
                },
                new Actor
                {
                 Id=2,
                 Name="Mila Kunis",
                 Biography="Ukranian",
                 DOB=new DateTime(1973,06,22),
                 Gender="Female"
                }
            };
            return list;
        }
        public static void MockGetById()
        {
                actorRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) =>
                 {
                     return ListOfActors().FirstOrDefault(a => a.Id == id);
                 }
                  );         
        }
        public static void MockAdd()
        {

            actorRepoMock.Setup(repo => repo.Add(It.IsAny<Actor>())).Returns(1);
        }
        public static void MockUpdate()
        {
            actorRepoMock.Setup(repo => repo.Update(It.IsAny<Actor>()));
        }
        public static void MockPatch()
        {
            actorRepoMock.Setup(repo => repo.Patch(It.IsAny<Actor>()));
        }
        public static void MockDelete()
        {
            actorRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
        public static void MockGetByMovieId()
        {
            actorRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>())).Returns((int movieId) =>
            {
                return ListOfActors();
            });
        }
    }
}
﻿using ImdbAPI.Models.DB;
using ImdbAPI.Repository;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImdbAPITests.MockResources
{
    public class ProducerMock
    {
        public static readonly Mock<IProducerRepository> producerRepoMock = new Mock<IProducerRepository>();
        public static void MockGet()
        {
            producerRepoMock.Setup(repo => repo.Get()).Returns(() =>
            {
                return ListOfProducers();
            });
        }
        private static IEnumerable<Producer> ListOfProducers()
        {
            var list = new List<Producer>()
            {
                new Producer()
                {
                 Id=1,
                 Name= "Albert Stotland Ruddy",
                 Biography = "Albert Stotland Ruddy is a Canadian-born film and television producer",
                 DOB =new DateTime(1930,03,28),
                 Gender="Male"
                },
                new Producer()
                {
                 Id=2,
                 Name= "Robert",
                 Biography ="Ukranian",
                 DOB=new DateTime(1973,06,22),
                 Gender="Male"
                }
            };
            return list;
        }
        public static void MockGetById()
        {
            producerRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) =>
            {
                return ListOfProducers().FirstOrDefault(a => a.Id == id);
            }
            );
        }
        public static void MockAdd()
        {
            producerRepoMock.Setup(repo => repo.Add(It.IsAny<Producer>())).Returns(1);
        }
        public static void MockUpdate()
        {
            producerRepoMock.Setup(repo => repo.Update(It.IsAny<Producer>()));
        }
        public static void MockDelete()
        {
            producerRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
    }
}
﻿using ImdbAPI.Models.DB;
using ImdbAPI.Repository;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace ImdbAPITests.MockResources
{
    public class MovieMock
    {
        public static readonly Mock<IMovieRepository> movieRepoMock = new Mock<IMovieRepository>();
        public static void MockGet()
        {
            movieRepoMock.Setup(repo => repo.Get()).Returns(() =>
            {
                return ListOfMovies();
            });
        }
        private static IEnumerable<Movie> ListOfMovies()
        {
            var list = new List<Movie>()
            {
       new Movie()
                {
                 Id=1,
                 Name= "Ford",
                 YearOfRelease=2021,
                 Plot= "Challenge Ferrari at the 24 Hours of Le Mans",
                 ProducerId=1,
                 Poster="https://m.media-amazon.com/images/M/MV5BM2UwMDVmMDItM2I2Yi00NGZmLTk4ZTUtY2JjNTQ3OGQ5ZjM2XkEyXkFqcGdeQXVyMTA1OTYzOTUx._V1_.jpg"

       },
        new Movie()
                {
                 Id=2,
                 Name= "Master",
                 YearOfRelease=2021,
                 Plot= "AAA",
                 ProducerId=1,
                 Poster="BBB.jpg"
       }
        };
            return list;
        }
        public static void MockGetByQuery()
        {
            movieRepoMock.Setup(repo => repo.GetByQuery(It.IsAny<Movie>())).Returns((Movie movie) =>
            {
                return ListOfMovies();
            });
        }
        public static void MockGetById()
        {
            movieRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) =>
            {
                return ListOfMovies().FirstOrDefault(a => a.Id == id);
            }
            );
        }
        public static void MockAdd()
        {
          movieRepoMock.Setup(repo => repo.Add(It.IsAny<Movie>(),It.IsAny<string>(),It.IsAny<string>()));
        }
        public static void MockUpdate()
        {
            movieRepoMock.Setup(repo => repo.Update(It.IsAny<Movie>(), It.IsAny<string>(), It.IsAny<string>()));
        }
        public static void MockDelete()
        {
            movieRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
    }
}
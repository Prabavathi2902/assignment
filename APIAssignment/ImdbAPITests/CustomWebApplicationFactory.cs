﻿using ImdbAPI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Hosting;
namespace ImdbAPITests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TestStartup>
    {
        protected override IHostBuilder CreateHostBuilder()
        {
            return base
                           .CreateHostBuilder()
            .ConfigureWebHostDefaults(webBuilder => {
                webBuilder.UseStartup<TestStartup>();
            });
        }
    }
}
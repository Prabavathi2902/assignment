﻿Feature: Genre Resource

Background: 
       Given I am a client

@GetAllGenres
Scenario: Get All Genres 
    When I make GET Request '<endPoint>'
    Then response code must be '<statusCode>'
    And response data must look like '<data>'

 Examples: 
    | endPoint | statusCode | data                                                |
    | genres   | 200        | [{"id":1,"name":"Action"},{"id":2,"name":"Comedy"}] |

@GetGenreById
Scenario: Get a Genre By Id
    When I make GET Request '<endPoint>'
    Then response code must be '<statusCode>'
    And response data must look like '<data>'

Examples: 
    | endPoint   | statusCode | data                                        |
    | genres/1   | 200        | {"id":1,"name":"Action"}                    |
    | genres/100 | 404        | {"message":"Genre not found with given Id"} |  

@AddGenre
Scenario: Add a Genre   
     When I making a POST request to '<endPoint>' with the following data '<data>' 
     Then response code must be '<statusCode>'
     And response data must look like '<message>'

Examples: 
    | endPoint | data              | statusCode | message                                     |
    | genres   | {"name":"Action"} | 200        | {"id":1}                                    |
    | genres   | {}                | 400        | {"message":"Genre name should not be null"} |

@UpdateGenre
Scenario: Update a Genre   
     When I make a PUT Request to '<endPoint>' with the following Data '<data>'
     Then response code must be '<statusCode>'
     And response data must look like '<response>'

Examples: 
    | endPoint   | data              | statusCode | response                                                   |
    | genres/1   | {"name":"Comedy"} | 200        | {"id":1}                                                   |
    | genres/100 | {"name":"Action"} | 404        | {"message":"Genre not found with given Id,Enter valid Id"} |
    | genres/1   | {}                | 400        | {"message":"Genre name should not be null"}                |

@DeleteGenres
Scenario: Remove a Genre
     When I make Delete Request to '<endPoint>'
     Then response code must be '<statusCode>'
     And response data must look like '<response>'

Examples: 
    | endPoint   | statusCode | response                                                   |
    | genres/1   | 200        | {"id":1}                                                   |
    | genres/100 | 404        | {"message":"Genre not found with given Id,Enter valid Id"} |

﻿Feature: Actor Resource

Background: 
       Given I am a client

@GetAllActors
Scenario: Get All Actors  
    When I make GET Request '<endPoint>'
    Then response code must be '<statusCode>'
    And response data must look like '<data>'

 Examples: 
    | endPoint | statusCode | data                                                                                                                                                                                                   |
    | actors   | 200        | [{"id":1,"name":"Christian Bale","biography":"British","dob":"1979-03-02T00:00:00","gender":"Male"},{"id":2,"name":"Mila Kunis","biography":"Ukranian","dob":"1973-06-22T00:00:00","gender":"Female"}] |

@GetActorById
Scenario: Get an Actor  
    When I make GET Request '<endPoint>'
    Then response code must be '<statusCode>'
    And response data must look like '<data>'

Examples: 
    | endPoint   | statusCode | data                                                                                               |
    | actors/1   | 200        | {"id":1,"name":"Christian Bale","biography":"British","dob":"1979-03-02T00:00:00","gender":"Male"} |
    | actors/100 | 404        | {"message":"Actor not found with given Id"}                                                        |

@AddActor
Scenario: Add an Actor   
     When I making a POST request to '<endPoint>' with the following data '<data>' 
     Then response code must be '<statusCode>'
     And response data must look like '<message>'

Examples: 
    | endPoint | data                                                                                  | statusCode | message                                                          |
    | actors   | {"name":"Vijay","biography":"Thalapathy","dob":"1979-06-22T00:00:00","gender":"Male"} | 200        | {"id":1}                       |
    | actors   | {"name":"Vijay","biography":"Thalapathy","dob":"1979-06-22T00:00:00" }                | 400        | {"message":"Actor name,Biography and Gender should not be null"} |
    | actors   | {"name":"Vijay","biography":"Thalapathy","dob":"2022-06-22T00:00:00","gender":"Male"} | 400        | {"message":"Enter valid DateOfBirth"}                            |

@UpdateActor
Scenario: Update an Actor   
     When I make a PUT Request to '<endPoint>' with the following Data '<data>'
     Then response code must be '<statusCode>'
     And response data must look like '<response>'

Examples: 
    | endPoint   | data                                                                                        | statusCode | response                                                         |
    | actors/1   | {"name":"Christian Bale","biography":"British","dob":"1979-03-02T00:00:00","gender":"Male"} | 200        | {"id":1}                                                         |
    | actors/100 | {"name":"Christian Bale","biography":"British","dob":"1979-03-02T00:00:00","gender":"Male"} | 404        | {"message":"Actor not found with given Id,Enter valid Id"}       |
    | actors/1   | {"name":"Vijay","biography":"Thalapathy","dob":"1979-06-22T00:00:00" }                      | 400        | {"message":"Actor name,Biography and Gender should not be null"} |
    | actors/1   | {"name":"Vijay","biography":"Thalapathy","dob":"2022-06-22T00:00:00","gender":"Male"}       | 400        | {"message":"Enter valid DateOfBirth"}                            |

@DeleteActor
Scenario: Remove an Actor
     When I make Delete Request to '<endPoint>'
     Then response code must be '<statusCode>'
     And response data must look like '<response>'

Examples: 
    | endPoint   | statusCode | response                                                   |
    | actors/1   | 200        | {"id":1}                                                   |
    | actors/100 | 404        | {"message":"Actor not found with given Id,Enter valid Id"} |

@PatchActor
Scenario: Patch Actor   
     When I make a Patch Request to '<endPoint>' with the following Data '<data>'
     Then response code must be '<statusCode>'
     And response data must look like '<response>'

Examples: 
    | endPoint   | data                                            | statusCode | response                                                   |
    | actors/2   | {"name":"Mila Kunis"}                           | 200        | {"id":2}                                                   |
    | actors/100 | {"name":"Christian Bale","biography":"British"} | 404        | {"message":"Actor not found with given Id,Enter valid Id"} |

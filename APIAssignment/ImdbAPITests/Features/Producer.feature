﻿Feature: Producer Resource

Background: 
       Given I am a client

@GetAllProducers
Scenario: Get All Producers  
    When I make GET Request '<endPoint>'
    Then response code must be '<statusCode>'
    And response data must look like '<data>'

 Examples: 
    | endPoint  | statusCode | data                                                                                                                                                                                                                                                                   |
    | producers | 200        | [{"id":1,"name":"Albert Stotland Ruddy","biography":"Albert Stotland Ruddy is a Canadian-born film and television producer","dob":"1930-03-28T00:00:00","gender":"Male"},{"id":2,"name":"Robert","biography":"Ukranian","dob":"1973-06-22T00:00:00","gender":"Male"}] |

@GetProducerById
Scenario: Get a  Producer  
    When I make GET Request '<endPoint>'
    Then response code must be '<statusCode>'
    And response data must look like '<data>'

Examples: 
    | endPoint      | statusCode | data                                                                                                                                                                    |
    | producers/1   | 200        | {"id":1,"name":"Albert Stotland Ruddy","biography":"Albert Stotland Ruddy is a Canadian-born film and television producer","dob":"1930-03-28T00:00:00","gender":"Male"} |
    | producers/100 | 404        | {"message":"Producer not found with given Id"}                                                                                                                          |

@AddProducer
Scenario: Add a Producer   
     When I making a POST request to '<endPoint>' with the following data '<data>' 
     Then response code must be '<statusCode>'
     And response data must look like '<message>'

Examples: 
    | endPoint  | data                                                                               | statusCode | message                                                             |
    | producers | {"name":"Rocky","biography":"Italian","dob":"1979-06-22T00:00:00","gender":"Male"} | 200        | {"id":1}                                                            |
    | producers | {"name":"Rocky","biography":"Italian","dob":"1979-06-22T00:00:00"}                 | 400        | {"message":"Producer name,Biography and Gender should not be null"} |
    | producers | {"name":"Rocky","biography":"Italian","dob":"2022-06-22T00:00:00","gender":"Male"} | 400        | {"message":"Enter valid DateOfBirth"}                               |             

@UpdateProducer
Scenario: Update a Producer   
     When I make a PUT Request to '<endPoint>' with the following Data '<data>'
     Then response code must be '<statusCode>'
     And response data must look like '<response>'

Examples: 
    | endPoint      | data                                                                                        | statusCode | response                                                            |
    | producers/1   | {"name":"Rocky Chartoff","biography":"Italian","dob":"1979-06-22T00:00:00","gender":"Male"} | 200        | {"id":1}                                                            |
    | producers/100 | {"name":"David","biography":"British","dob":"1979-03-02T00:00:00","gender":"Male"}          | 404        | {"message":"Producer not found with given Id,Enter valid Id"}       |
    | producers/1   | {"name":"Rocky Chartoff","biography":"Italian","dob":"1979-06-22T00:00:00" }                | 400        | {"message":"Producer name,Biography and Gender should not be null"} |
    | producers/1   | {"name":"Rocky Chartoff","biography":"Italian","dob":"2022-06-22T00:00:00","gender":"Male"} | 400        | {"message":"Enter valid DateOfBirth"}                               |


@DeleteProducer
Scenario: Remove a Producer
     When I make Delete Request to '<endPoint>'
     Then response code must be '<statusCode>'
     And response data must look like '<response>'

Examples: 
    | endPoint      | statusCode | response                                                      |
    | producers/1   | 200        | {"id":1}                                                      |
    | producers/100 | 404        | {"message":"Producer not found with given Id,Enter valid Id"} |
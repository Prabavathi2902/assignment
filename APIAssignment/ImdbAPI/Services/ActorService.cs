﻿using ImdbAPI.CustomExceptions;
using ImdbAPI.Models.DB;
using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using ImdbAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
namespace ImdbAPI.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;
        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }
        public int Create(ActorRequest actorRequest)
        {
            if(string.IsNullOrEmpty(actorRequest.Name)|| string.IsNullOrEmpty(actorRequest.Biography) || string.IsNullOrEmpty(actorRequest.Gender))
            {
                throw new ArgumentException("Actor name,Biography and Gender should not be null");
            }
            else if(actorRequest.DOB > DateTime.Today || actorRequest.DOB == DateTime.MinValue)
            {
                throw new ArgumentException("Enter valid DateOfBirth");
            }
            var actor = new Actor
            {
                Name = actorRequest.Name,
                Biography = actorRequest.Biography,
                DOB = actorRequest.DOB,
                Gender = actorRequest.Gender
            };
            return _actorRepository.Add(actor);
        }
        public void Delete(int id)
        {
            var actor = _actorRepository.GetById(id);
            if (actor == null)
            {
                throw new EntityDoesNotExistException("Actor not found with given Id,Enter valid Id");
            }
            _actorRepository.Delete(id);
        }
        public IEnumerable<ActorResponse> Get()
        {
            return _actorRepository.Get().Select(actor => new ActorResponse
            {
                Id = actor.Id,
                Name = actor.Name,
                Biography = actor.Biography,
                DOB = actor.DOB,
                Gender = actor.Gender
            });
        }
        public ActorResponse GetById(int id)
        {
            var actor = _actorRepository.GetById(id);
            if (actor == null)
            {
                return null;
            }
            return new ActorResponse
            {
                Id = actor.Id,
                Name = actor.Name,
                Biography = actor.Biography,
                DOB = actor.DOB,
                Gender = actor.Gender
            };
        }

        public void Patch(int id, ActorRequest actorRequest)
        {
            var actors = _actorRepository.GetById(id);
            if (actors == null)
            {
                throw new EntityDoesNotExistException("Actor not found with given Id,Enter valid Id");
            }
            var actor = new Actor
            {
                Id = id,
                Name = actorRequest.Name,
                Biography = actorRequest.Biography,
                DOB = actorRequest.DOB,
                Gender = actorRequest.Gender
            };
            _actorRepository.Patch(actor);
        }

        public void Update(int id, ActorRequest actorRequest)
        {
            var actors = _actorRepository.GetById(id);
            if (actors == null)
            {
                throw new EntityDoesNotExistException("Actor not found with given Id,Enter valid Id");
            }
            if (string.IsNullOrEmpty(actorRequest.Name) || string.IsNullOrEmpty(actorRequest.Biography) || string.IsNullOrEmpty(actorRequest.Gender))
            {
                throw new ArgumentException("Actor name,Biography and Gender should not be null");
            }
            if (actorRequest.DOB > DateTime.Today || actorRequest.DOB == DateTime.MinValue)
            {
                throw new ArgumentException("Enter valid DateOfBirth");
            }
            var actor = new Actor
            {
                Id = id,
                Name = actorRequest.Name,
                Biography = actorRequest.Biography,
                DOB = actorRequest.DOB,
                Gender = actorRequest.Gender
            };
            _actorRepository.Update(actor);
        }
    }
}
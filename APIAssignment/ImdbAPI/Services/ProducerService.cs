﻿using ImdbAPI.CustomExceptions;
using ImdbAPI.Models.DB;
using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using ImdbAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
namespace ImdbAPI.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;
        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;
        }
        public int Create(ProducerRequest producerRequest)
        {
            if (string.IsNullOrEmpty(producerRequest.Name) || string.IsNullOrEmpty(producerRequest.Biography) || string.IsNullOrEmpty(producerRequest.Gender))
            {
                throw new ArgumentException("Producer name,Biography and Gender should not be null");
            }
            else if (producerRequest.DOB > DateTime.Today || producerRequest.DOB == DateTime.MinValue)
            {
                throw new ArgumentException("Enter valid DateOfBirth");
            }
            var producer = new Producer
            {
                Name = producerRequest.Name,
                Biography = producerRequest.Biography,
                DOB = producerRequest.DOB,
                Gender = producerRequest.Gender
            };
            return _producerRepository.Add(producer);
        }
        public void Delete(int id)
        {
            var producer = _producerRepository.GetById(id);
            if (producer == null)
            {
                throw new EntityDoesNotExistException("Producer not found with given Id,Enter valid Id");
            }
            _producerRepository.Delete(id);
        }
        public IEnumerable<ProducerResponse> Get()
        {
            return _producerRepository.Get().Select(producer => new ProducerResponse
            {
                Id = producer.Id,
                Name = producer.Name,
                Biography = producer.Biography,
                DOB = producer.DOB,
                Gender = producer.Gender
            });
        }
        public ProducerResponse GetById(int id)
        {
            var producer = _producerRepository.GetById(id);
            if (producer == null)
            {
                return null;
            }
            return new ProducerResponse
            {
                Id = producer.Id,
                Name = producer.Name,
                Biography = producer.Biography,
                DOB = producer.DOB,
                Gender = producer.Gender
            };
        }
        public void Update(int id, ProducerRequest producerRequest)
        {
            var producers = _producerRepository.GetById(id);
            if (producers == null)
            {
                throw new EntityDoesNotExistException("Producer not found with given Id,Enter valid Id");
            }
            if (string.IsNullOrEmpty(producerRequest.Name) || string.IsNullOrEmpty(producerRequest.Biography) || string.IsNullOrEmpty(producerRequest.Gender))
            {
                throw new ArgumentException("Producer name,Biography and Gender should not be null");
            }
            if (producerRequest.DOB > DateTime.Today || producerRequest.DOB == DateTime.MinValue)
            {
                throw new ArgumentException("Enter valid DateOfBirth");
            }
            var producer = new Producer
            {
                Id = id,
                Name = producerRequest.Name,
                Biography = producerRequest.Biography,
                DOB = producerRequest.DOB,
                Gender = producerRequest.Gender
            };
            _producerRepository.Update(producer);
        }
    }
}
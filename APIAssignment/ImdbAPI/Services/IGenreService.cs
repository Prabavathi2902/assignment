﻿using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using System.Collections.Generic;
namespace ImdbAPI.Services
{
    public interface IGenreService
    {
        public int Create(GenreRequest genreRequest);
        public IEnumerable<GenreResponse> Get();
        public GenreResponse GetById(int id);
        public void Delete(int id);
        public void Update(int id, GenreRequest genreRequest);
    }
}
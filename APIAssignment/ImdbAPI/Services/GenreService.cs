﻿using ImdbAPI.CustomExceptions;
using ImdbAPI.Models.DB;
using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using ImdbAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
namespace ImdbAPI.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;
        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }
        public int Create(GenreRequest genreRequest)
        {
            if (string.IsNullOrEmpty(genreRequest.Name))
            {
                throw new ArgumentException("Genre name should not be null");
            }
            var genre = new Genre
            {
                Name = genreRequest.Name,
            };
            return _genreRepository.Add(genre);
        }
        public void Delete(int id)
        {
            var genre = _genreRepository.GetById(id);
            if (genre == null)
            {
                throw new EntityDoesNotExistException("Genre not found with given Id,Enter valid Id");
            }
            _genreRepository.Delete(id);
        }
        public IEnumerable<GenreResponse> Get()
        {
            return _genreRepository.Get().Select(genre => new GenreResponse
            {
                Id = genre.Id,
                Name = genre.Name,
            });
        }
        public GenreResponse GetById(int id)
        {
            var genre = _genreRepository.GetById(id);
            if (genre == null)
            {
                return null;
            }
            return new GenreResponse
            {
                Id = genre.Id,
                Name = genre.Name,
            };
        }
        public void Update(int id, GenreRequest genreRequest)
        {
            var genres = _genreRepository.GetById(id);
            if (genres == null)
            {
                throw new EntityDoesNotExistException("Genre not found with given Id,Enter valid Id");
            }
            if (string.IsNullOrEmpty(genreRequest.Name))
            {
                throw new ArgumentException("Genre name should not be null");
            }
            var genre = new Genre
            {
                Id = id,
                Name = genreRequest.Name,
            };
            _genreRepository.Update(genre);
        }
    }
}
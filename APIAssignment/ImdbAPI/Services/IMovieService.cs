﻿using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using System.Collections.Generic;
namespace ImdbAPI.Services
{
    public interface IMovieService
    {       
        public IEnumerable<MovieResponse> Get(MovieRequest movieRequest);
        public void Create(MovieRequest movieRequest);
        public MovieResponse GetById(int id);
        public void Delete(int id);
        public void Update(int id, MovieRequest movieRequest);
    }
}
﻿using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using System.Collections.Generic;
namespace ImdbAPI.Services
{
    public interface IProducerService
    {
        public int Create(ProducerRequest producerRequest);
        public IEnumerable<ProducerResponse> Get();
        public ProducerResponse GetById(int id);
        public void Delete(int id);
        public void Update(int id, ProducerRequest producerRequest);
    }
}
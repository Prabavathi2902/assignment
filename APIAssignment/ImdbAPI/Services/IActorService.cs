﻿using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using System.Collections.Generic;
namespace ImdbAPI.Services
{
    public interface IActorService
    {
        public int Create(ActorRequest actorRequest);
        public IEnumerable<ActorResponse> Get();
        public ActorResponse GetById(int id);
        public void Delete(int id);
        public void Update(int id,ActorRequest actorRequest);
        public void Patch(int id,ActorRequest actorRequest);
    }
}
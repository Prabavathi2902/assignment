﻿using Dapper;
using ImdbAPI.CustomExceptions;
using ImdbAPI.Models.DB;
using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using ImdbAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
namespace ImdbAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IProducerRepository _producerRepository;
        private readonly IGenreRepository _genreRepository;
        public MovieService(IMovieRepository movieRepository, IActorRepository actorRepository, IProducerRepository producerRepository, IGenreRepository genreRepository)
        {
            _movieRepository = movieRepository;
            _actorRepository = actorRepository;
            _producerRepository = producerRepository;
            _genreRepository = genreRepository;
        }
        public void Create(MovieRequest movieRequest)
        {
            if (string.IsNullOrEmpty(movieRequest.Name) || string.IsNullOrEmpty(movieRequest.Plot) || string.IsNullOrEmpty(movieRequest.Poster) || movieRequest.YearOfRelease == 0 || movieRequest.ProducerId == 0)
            {
                throw new ArgumentException("Movie name,YearOfRelease,Plot,Poster and ProducerId should not be null");
            }
            if (movieRequest.ActorIds.Count == 0 || movieRequest.GenreIds.Count == 0)
            {
                throw new ArgumentException("ActorIds and GenreIds should not be empty");
            }
            movieRequest.ActorIds.ForEach(x =>
            {
                var actor = _actorRepository.GetById(x);
                if (actor == null)
                {
                    throw new EntityDoesNotExistException("Actor not found with given Id,Enter valid Id");
                }
            }
            );
            movieRequest.GenreIds.ForEach(x =>
            {
                var genre = _genreRepository.GetById(x);
                if (genre == null)
                {
                    throw new EntityDoesNotExistException("Genre not found with given Id,Enter valid Id");
                }
            }
            );
            _movieRepository.Add(new Movie
            {

                Name = movieRequest.Name,
                YearOfRelease = movieRequest.YearOfRelease,
                Plot = movieRequest.Plot,
                Poster = movieRequest.Poster,
                ProducerId = movieRequest.ProducerId
            },
            string.Join(',', movieRequest.ActorIds),
            string.Join(',', movieRequest.GenreIds)
           );
        }
        public void Delete(int id)
        {
            var movie = _movieRepository.GetById(id);
            if (movie == null)
            {
                throw new EntityDoesNotExistException("Movie not found with given Id,Enter valid Id");
            }
            _movieRepository.Delete(id);
        }
        public IEnumerable<MovieResponse> Get(MovieRequest movieRequest)
        {
            var movie = new Movie
            {
                Name = movieRequest.Name,
                YearOfRelease = movieRequest.YearOfRelease,
                Plot = movieRequest.Plot,
                ProducerId = movieRequest.ProducerId,
                Poster = movieRequest.Poster
            };
            if (string.IsNullOrEmpty(movieRequest.Name) && string.IsNullOrEmpty(movieRequest.Plot) && string.IsNullOrEmpty(movieRequest.Poster) && movieRequest.YearOfRelease == 0 && movieRequest.ProducerId == 0)
            {
                return _movieRepository.Get().Select(movie => new MovieResponse
                {
                    Id = movie.Id,
                    Name = movie.Name,
                    YearOfRelease = movie.YearOfRelease,
                    Plot = movie.Plot,
                    Actors = _actorRepository.GetByMovieId(movie.Id),
                    Producer = _producerRepository.GetById(movie.ProducerId),
                    CoverImage = movie.Poster,
                    Genres = _genreRepository.GetByMovieId(movie.Id)
                });
            }
            if (movie.ProducerId != 0)
            {
                var producer = _producerRepository.GetById(movieRequest.ProducerId);
                if (producer == null)
                {
                    throw new EntityDoesNotExistException("Producer not found with given Id,Enter valid Id");
                }
            }
            return _movieRepository.GetByQuery(movie).Select(movie => new MovieResponse
            {
                Id = movie.Id,
                Name = movie.Name,
                YearOfRelease = movie.YearOfRelease,
                Plot = movie.Plot,
                Actors = _actorRepository.GetByMovieId(movie.Id),
                Producer = _producerRepository.GetById(movie.ProducerId),
                CoverImage = movie.Poster,
                Genres = _genreRepository.GetByMovieId(movie.Id)
            });
        }
        public MovieResponse GetById(int id)
        {
            var movie = _movieRepository.GetById(id);
            if (movie == null)
            {
                return null;
            }
            return new MovieResponse
            {
                Id = movie.Id,
                Name = movie.Name,
                YearOfRelease = movie.YearOfRelease,
                Plot = movie.Plot,
                Actors = _actorRepository.GetByMovieId(movie.Id),
                Producer = _producerRepository.GetById(movie.ProducerId),
                CoverImage = movie.Poster,
                Genres = _genreRepository.GetByMovieId(movie.Id)
            };
        }
        public void Update(int id, MovieRequest movieRequest)
        {
            var movies = _movieRepository.GetById(id);
            if (movies == null)
            {
                throw new EntityDoesNotExistException("Movie not found with given Id,Enter valid Id");
            }
            if (string.IsNullOrEmpty(movieRequest.Name) || string.IsNullOrEmpty(movieRequest.Plot) || string.IsNullOrEmpty(movieRequest.Poster) || movieRequest.YearOfRelease == 0 || movieRequest.ProducerId == 0)
            {
                throw new ArgumentException("Movie name,YearOfRelease,Plot,Poster and ProducerId should not be null");
            }
            if (movieRequest.ActorIds.Count == 0 || movieRequest.GenreIds.Count == 0)
            {
                throw new ArgumentException("ActorIds and GenreIds should not be empty");
            }
            movieRequest.ActorIds.ForEach(x =>
            {
                var actor = _actorRepository.GetById(x);
                if (actor == null)
                {
                    throw new EntityDoesNotExistException("Actor not found with given Id,Enter valid Id");
                }
            }
           );
            movieRequest.GenreIds.ForEach(x =>
            {
                var genre = _genreRepository.GetById(x);
                if (genre == null)
                {
                    throw new EntityDoesNotExistException("Genre not found with given Id,Enter valid Id");
                }
            }
            );
            var movie = new Movie
            {
                Id = id,
                Name = movieRequest.Name,
                YearOfRelease = movieRequest.YearOfRelease,
                Plot = movieRequest.Plot,
                ProducerId = movieRequest.ProducerId,
                Poster = movieRequest.Poster
            };
            string ActorIds = string.Join(',', movieRequest.ActorIds);
            string GenreIds = string.Join(',', movieRequest.GenreIds);
            _movieRepository.Update(movie, ActorIds, GenreIds);
        }
    }
}
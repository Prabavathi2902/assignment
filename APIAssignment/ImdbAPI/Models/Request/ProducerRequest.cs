﻿using System;
namespace ImdbAPI.Models.Request
{
    public class ProducerRequest
    {      
        public string Name { get; set; }
        public string Biography { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
    }
}
﻿using System;
namespace ImdbAPI.Models.Response
{
    public class ProducerResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Biography { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
    }
}
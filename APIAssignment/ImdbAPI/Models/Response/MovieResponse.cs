﻿using ImdbAPI.Models.DB;
using System.Collections.Generic;
namespace ImdbAPI.Models.Response
{
    public class MovieResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        public IEnumerable<Actor> Actors { get; set; }
        public IEnumerable<Genre> Genres { get; set; }
        public Producer Producer { get; set; }
        public string CoverImage { get; set; }
    }
}
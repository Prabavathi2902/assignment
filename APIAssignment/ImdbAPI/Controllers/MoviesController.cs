﻿using ImdbAPI.CustomExceptions;
using ImdbAPI.Models.Request;
using ImdbAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ImdbAPI.Controllers
{
    [Route("[Controller]")]
    public class MoviesController : BaseController
    {
        private readonly IMovieService _movieService;
        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }
        [HttpGet]
        public IActionResult Get([FromQuery] MovieRequest movieRequest)
        {
            try
            {
                var movies = _movieService.Get(movieRequest);
                return new JsonResult(movies);
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
        }
        [HttpPost]
        public IActionResult Post([FromBody] MovieRequest movieRequest)
        {
            try
            {
                _movieService.Create(movieRequest);
                return Ok(new { message = "Movie got added successfully" });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
            catch (ArgumentException e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var movies = _movieService.GetById(id);
            if (movies == null)
            {
                return NotFound(new { message = "Movie not found with given Id" });
            }
            return new JsonResult(movies);
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _movieService.Delete(id);
                return Ok(new { Id = id });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
        }
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] MovieRequest movieRequest)
        {
            try
            {
                _movieService.Update(id, movieRequest);
                return Ok(new { Id = id });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
            catch (ArgumentException e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
    }
}
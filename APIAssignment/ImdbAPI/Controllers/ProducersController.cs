﻿using ImdbAPI.CustomExceptions;
using ImdbAPI.Models.Request;
using ImdbAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ImdbAPI.Controllers
{
    [Route("[Controller]")]
    public class ProducersController : BaseController
    {
        private readonly IProducerService _producerService;
        public ProducersController(IProducerService producerService)
        {
            _producerService = producerService;
        }
        [HttpGet]
        public IActionResult Get()
        {
            var producers = _producerService.Get();
            return new JsonResult(producers);
        }
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var producers = _producerService.GetById(id);
            if (producers == null)
            {
                return NotFound(new { message = "Producer not found with given Id" });
            }
            return new JsonResult(producers);
        }
        [HttpPost]
        public IActionResult Post([FromBody] ProducerRequest producerRequest)
        {
            try
            {
                int id=_producerService.Create(producerRequest);
                return Ok(new { Id=id });
            }
            catch (ArgumentException e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _producerService.Delete(id);
                return Ok(new { Id = id });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }      
        }
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] ProducerRequest producerRequest)
        {
            try
            {
                _producerService.Update(id, producerRequest);
                return Ok(new { Id = id });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
            catch (ArgumentException e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
    }
}
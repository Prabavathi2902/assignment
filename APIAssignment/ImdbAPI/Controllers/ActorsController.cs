﻿using ImdbAPI.CustomExceptions;
using ImdbAPI.Models.Request;
using ImdbAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace ImdbAPI.Controllers
{
    [Route("[Controller]")]
    public class ActorsController : BaseController
    {
        private readonly IActorService _actorService;
        public ActorsController(IActorService actorService)
        {
            _actorService = actorService;
        }
        [HttpGet]
        public IActionResult Get()
        {
            var actors = _actorService.Get();
            return new JsonResult(actors);
        }
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {          
            var actors = _actorService.GetById(id);
            if (actors == null)
            {
                return NotFound(new {message= "Actor not found with given Id"});
            }
            return new JsonResult(actors);
        }
        [HttpPost]
        public IActionResult Post([FromBody] ActorRequest actorRequest)
        {
            try
            {
                int id=_actorService.Create(actorRequest);
                return Ok(new {Id=id});
            }
            catch (ArgumentException e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _actorService.Delete(id);
                return Ok(new { Id = id });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
        }
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] ActorRequest actorRequest)
        {
            try
            {
                _actorService.Update(id, actorRequest);
                return Ok(new { Id = id });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
            catch (ArgumentException e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
        [HttpPatch("{id}")]
        public IActionResult Patch(int id, [FromBody] ActorRequest actorRequest)
        {
            try
            {
                _actorService.Patch(id, actorRequest);
                return Ok(new { Id = id });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
        }
    }
}
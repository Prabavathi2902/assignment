﻿using ImdbAPI.CustomExceptions;
using ImdbAPI.Models.Request;
using ImdbAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ImdbAPI.Controllers
{
    [Route("[Controller]")]
    public class GenresController : BaseController
    {
        private readonly IGenreService _genreService;
        public GenresController(IGenreService genreService)
        {
            _genreService = genreService;
        }
        [HttpGet]
        public IActionResult Get()
        {
            var genres = _genreService.Get();
            return new JsonResult(genres);
        }
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var genres = _genreService.GetById(id);
            if (genres == null)
            {
                return NotFound(new { message = "Genre not found with given Id" });
            }
            return new JsonResult(genres);
        }
        [HttpPost]
        public IActionResult Post([FromBody] GenreRequest genreRequest)
        {
            try
            {
                int id=_genreService.Create(genreRequest);
                return Ok(new { Id=id });
            }
            catch (ArgumentException e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _genreService.Delete(id);
                return Ok(new { Id = id });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
        }
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] GenreRequest genreRequest)
        {
            try
            {
                _genreService.Update(id, genreRequest);
                return Ok(new { Id = id });
            }
            catch (EntityDoesNotExistException e)
            {
                return NotFound(new { message = e.Message });
            }
            catch (ArgumentException e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
    }
}
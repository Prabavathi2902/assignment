﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImdbAPI.CustomExceptions
{
    public class EntityDoesNotExistException : Exception
    {
        public EntityDoesNotExistException(string message)
            :base(message)
        {

        }
    }
}

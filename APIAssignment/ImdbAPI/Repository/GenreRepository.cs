﻿using ImdbAPI.Models.DB;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
namespace ImdbAPI.Repository
{
	public class GenreRepository : GenericRepository<Genre>, IGenreRepository
	{
		public GenreRepository(IOptions<ConnectionString> connectionString)
	   : base(connectionString.Value)
		{
		}
		public int Add(Genre genre)
		{
			string sql = @"
INSERT INTO Genres (NAME)
VALUES (@Name);
SELECT CAST(SCOPE_IDENTITY() AS INT);";
			return Add(sql, genre);
		}
		public void Delete(int id)
		{
			string sql = @"
DELETE Genres
WHERE Id = @Id
";
			Delete(sql, id);
		}
		public IEnumerable<Genre> Get()
		{
			const string sql = @"
SELECT *
FROM Genres
";
			return Get(sql);
		}
		public Genre GetById(int id)
		{
			string sql = @"
SELECT Id
	,Name
FROM Genres
WHERE Id = @Id
";
			return GetById(sql, id);
		}
		public void Update(Genre genre)
		{
			string sql = @"
UPDATE Genres
SET Name = @Name
WHERE Id = @id
";
			Update(sql, genre);
		}
		public IEnumerable<Genre> GetByMovieId(int id)
		{
			const string sql = @"
SELECT G.*
FROM Genres G
INNER JOIN MovieGenreMapping MG ON G.Id = MG.GenreId
WHERE MG.MovieId = @id
";
			return GetByMovieId(sql, id);
		}
	}
}
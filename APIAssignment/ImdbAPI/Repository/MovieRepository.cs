﻿using Dapper;
using ImdbAPI.Models.DB;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ImdbAPI.Repository
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        public MovieRepository(IOptions<ConnectionString> connectionString)
: base(connectionString.Value)
        {
        }
        public IEnumerable<Movie> Get()
        {
            const string sql = @"
SELECT *
FROM Movies
";
            return Get(sql);
        }
        public IEnumerable<Movie> GetByQuery(Movie movie)
        {
            var sql = @"
SELECT *
FROM Movies
WHERE 1=1
";
            DynamicParameters parameter = new DynamicParameters();
            if (movie.Name != null)
            {
                sql += "AND Name=@Name";
                parameter.Add("@Name", movie.Name);
            }
            if (movie.Plot != null)
            {
                sql += " AND Plot=@Plot";
                parameter.Add("@Plot", movie.Plot);
            }
            if (movie.Poster != null)
            {
                sql += " AND Poster=@Poster";
                parameter.Add("@Poster", movie.Poster);
            }
            if (movie.YearOfRelease != 0)
            {
                sql += " AND YearOfRelease=@YearOfRelease";
                parameter.Add("@YearOfRelease", movie.YearOfRelease);
            }
            if (movie.ProducerId != 0)
            {
                sql += " AND ProducerId=@ProducerId";
                parameter.Add("@ProducerId", movie.ProducerId);
            }           
            return GetAll(sql,parameter);
        }
        public void Add(Movie movie, string ActorIds, string GenreIds)
        {
            var procedure = "[usp_AddMovie]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Name",movie.Name);
            parameter.Add("@Plot", movie.Plot);
            parameter.Add("@YearOfRelease", movie.YearOfRelease);
            parameter.Add("@Poster", movie.Poster);
            parameter.Add("@ProducerId", movie.ProducerId);
            parameter.Add("@ActorIds", ActorIds);
            parameter.Add("@GenreIds", GenreIds);
            AddUpdateMovie(procedure,parameter);
        }
        public void Delete(int id)
        {
            var sql = @"
DELETE MovieActorMapping
WHERE MovieId = @id

DELETE MovieGenreMapping
WHERE MovieId = @id

DELETE Movies
WHERE Id = @id
";
            Delete(sql, id);
        }
        public Movie GetById(int id)
        {
            string sql = @"
SELECT *
FROM Movies
WHERE Id = @id
";
            return GetById(sql, id);
        }
        public void Update(Movie movie, string ActorIds, string GenreIds)
        {
            var procedure = "[usp_UpdateMovies]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Id", movie.Id);
            parameter.Add("@Name", movie.Name);
            parameter.Add("@Plot", movie.Plot);
            parameter.Add("@YOR", movie.YearOfRelease);
            parameter.Add("@Image", movie.Poster);
            parameter.Add("@PId", movie.ProducerId);
            parameter.Add("@ActorIds", ActorIds);
            parameter.Add("@GenreIds", GenreIds);
            AddUpdateMovie(procedure, parameter);
        }
    }
}
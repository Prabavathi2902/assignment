﻿using ImdbAPI.Models.DB;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
namespace ImdbAPI.Repository
{
    public class ProducerRepository : GenericRepository<Producer>, IProducerRepository
    {
        public ProducerRepository(IOptions<ConnectionString> connectionString)
: base(connectionString.Value)
        {
        }
        public int Add(Producer producer)
        {
            string sql = @"
INSERT INTO Producers (
    NAME
    ,Gender
    ,DOB
    ,Biography
    )
VALUES (
    @Name
    ,@Gender
    ,@DOB
    ,@Biography);
SELECT CAST(SCOPE_IDENTITY() AS INT);";
            return Add(sql, producer);
        }
        public void Delete(int id)
        {
            string sql = @"
DELETE Producers
WHERE Id = @Id
";
            Delete(sql, id);
        }
        public IEnumerable<Producer> Get()
        {
            const string sql = @"
SELECT *
FROM Producers
";
            return Get(sql);
        }
        public Producer GetById(int id)
        {
            string sql = @"
SELECT Id
    ,Name
    ,DOB
    ,Biography
    ,Gender
FROM Producers
WHERE Id = @Id
";
            return GetById(sql, id);
        }
        public void Update(Producer producer)
        {
            string sql = @"
UPDATE Producers
SET Name = @Name
    ,Gender = @Gender
    ,DOB = @DOB
    ,Biography = @Biography
WHERE Id = @id
";
            Update(sql, producer);
        }
    }
}
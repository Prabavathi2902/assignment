﻿using ImdbAPI.Models.DB;
using System.Collections.Generic;
namespace ImdbAPI.Repository
{
    public interface IActorRepository
    {
        public int Add(Actor actor);
        public IEnumerable<Actor> Get();
        public Actor GetById(int id);
        public void Delete(int id);
        public void Update(Actor actor);
        public void Patch(Actor actor);
        public IEnumerable<Actor> GetByMovieId(int id);
    }
}
﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace ImdbAPI.Repository
{
    public class GenericRepository<T>
    {
        private readonly ConnectionString _connectionString;
        public GenericRepository(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }
        public int Add(string sql, T entity)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            var id=connection.QueryFirst<int>(sql, entity);
            return id;
        }
        public void Delete(string sql, int id)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Execute(sql, new { Id = id });
        }
        public IEnumerable<T> Get(string sql)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            return connection.Query<T>(sql);
        }
        public T GetById(string sql, int id)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            return connection.QueryFirstOrDefault<T>(sql, new { Id = id });
        }
        public void Update(string sql, T entity)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Execute(sql, entity);
        }
        public IEnumerable<T> GetByMovieId(string sql, int id)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            return connection.Query<T>(sql, new { Id = id });
        }
        public void AddUpdateMovie(string sql, DynamicParameters parameter)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Execute(sql,parameter, commandType: CommandType.StoredProcedure);
        }
        public IEnumerable<T> GetAll(string sql, DynamicParameters parameter)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            return connection.Query<T>(sql,parameter);
        }
        public void Patch(string sql, DynamicParameters parameter)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Execute(sql, parameter);
        }
    }
}
﻿using ImdbAPI.Models.DB;
using System.Collections.Generic;
namespace ImdbAPI.Repository
{
    public interface IGenreRepository
    {
        public int Add(Genre genre);
        public IEnumerable<Genre> Get();
        public Genre GetById(int id);
        public void Delete(int id);
        public void Update(Genre genre);
        public IEnumerable<Genre> GetByMovieId(int id);
    }
}
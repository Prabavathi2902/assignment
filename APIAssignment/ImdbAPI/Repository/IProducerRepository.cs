﻿using ImdbAPI.Models.DB;
using System.Collections.Generic;
namespace ImdbAPI.Repository
{
    public interface IProducerRepository
    {
        public int Add(Producer producer);
        public IEnumerable<Producer> Get();
        public Producer GetById(int id);
        public void Delete(int id);
        public void Update(Producer producer);
    }
}
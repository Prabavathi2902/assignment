﻿using ImdbAPI.Models.DB;
using System.Collections.Generic;
namespace ImdbAPI.Repository
{
    public interface IMovieRepository
    {
        public IEnumerable<Movie> GetByQuery(Movie movie);
        public IEnumerable<Movie> Get();
        public void Add(Movie movie,string ActorIds,string GenreIds);
        public void Delete(int id);
        public Movie GetById(int id);
        public void Update( Movie movie, string ActorIds, string GenreIds);
    }
}
﻿using Dapper;
using ImdbAPI.Models.DB;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
namespace ImdbAPI.Repository
{
    public class ActorRepository : GenericRepository<Actor>, IActorRepository
    {
        public ActorRepository(IOptions<ConnectionString> connectionString)
  : base(connectionString.Value)
        {
        }
        public int Add(Actor actor)
        {
            string sql = @"
INSERT INTO Actors (
    NAME
    ,Gender
    ,DOB
    ,Biography
    )
VALUES (
    @Name
    ,@Gender
    ,@DOB
    ,@Biography
    );
SELECT CAST(SCOPE_IDENTITY() AS INT);";
            return Add(sql, actor);
        }
        public void Delete(int id)
        {

            string sql = @"
DELETE Actors
WHERE Id = @Id
";
            Delete(sql, id);
        }
        public IEnumerable<Actor> Get()
        {
            const string sql = @"
SELECT *
FROM Actors
";
            return Get(sql);
        }
        public Actor GetById(int id)
        {
            string sql = @"
SELECT Id
    ,Name
    ,DOB
    ,Biography
    ,Gender
FROM Actors
WHERE Id = @Id
";
        return GetById(sql, id);
        }
        public void Update( Actor actor)
        {
            string sql = @"
UPDATE Actors
SET Name = @Name
    ,Gender = @Gender
    ,DOB = @DOB
    ,Biography = @Biography
WHERE Id = @Id
";
            Update(sql, actor);
        }
        public IEnumerable<Actor> GetByMovieId(int id)
        {
            const string sql = @"
SELECT A.*
FROM Actors A
INNER JOIN MovieActorMapping MA
ON A.Id = MA.ActorId
WHERE MA.MovieId = @id
";
            return GetByMovieId(sql, id);
        }
        public void Patch(Actor actor)
        {
            var sql = @"
UPDATE Actors
SET
";
            List<string> query = new List<string>();
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@id", actor.Id);
            if (actor.Name != null)
            {
                query.Add("Name=@Name");
                parameter.Add("@Name", actor.Name);
            }
            if (actor.Biography != null)
            {
                query.Add("Biography=@Biography");
                parameter.Add("@Biography", actor.Biography);
            }
            if (actor.Gender != null)
            {
                query.Add("Gender =@Gender");
                parameter.Add("@Gender", actor.Gender);
            }
            if (!(actor.DOB > DateTime.Today))
            {
                query.Add("DOB =@DOB");
                parameter.Add("@DOB", actor.DOB);
            }
            var result = String.Join(",", query);
            sql += result;
            sql += " WHERE Id = @id";
            Patch(sql, parameter);

        }
    }
}
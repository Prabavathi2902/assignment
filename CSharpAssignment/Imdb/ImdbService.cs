﻿using Imdb.Repository;
using Imdb.Domain;
using System;
using System.Collections.Generic;

namespace Imdb
{
    public class ImdbService
    {
        private readonly MovieRepository _movieRepository ;
        private readonly ActorRepository _actorRepository;
        private readonly ProducerRepository _producerRepository;

        public ImdbService()
        {
            _movieRepository = new MovieRepository();
            _actorRepository = new ActorRepository();
            _producerRepository = new ProducerRepository();
        }

        public void AddMovie(string name, int year, string plot,List<Person> actors,Person producer)
        {
           
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(plot)  || string.IsNullOrEmpty(producer.Name)||actors.Count==0)
            {
                throw new ArgumentException("Invalid arguments");
            }

            var movie = new Movie()
            {
                Name = name,
                Year = year,
                Plot = plot,
                Actors=actors,
                Producer=producer
                
            };

            _movieRepository.Add(movie);
        }
        public List<Movie> GetMovies()
        {
            return _movieRepository.Get();
        }

        public void DeleteMovie(string name)
        {

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid arguments");
            }


            _movieRepository.Remove(name);
        }

        public void AddActor(string name, string dob)
        {
            
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(dob))
            {
                throw new ArgumentException("Actor name and dob should not be null");
            }
            //else if (DateTime.Parse(dob).CompareTo(DateTime.Today) > 0)
            //{
            //    throw new ArgumentException("Enter valid DateOfBirth");
            //}
            DateTime dateOfBirth = DateTime.ParseExact(dob, "yyyy/MM/dd", null);
            if (dateOfBirth > DateTime.Today|| dateOfBirth == DateTime.MinValue)
            {
                throw new ArgumentException("Enter valid DateOfBirth");
            }
            
            var actor = new Person()
            {
                Name = name,
                DateOfBirth=dateOfBirth
            };

            _actorRepository.Add(actor);
        }
        public List<Person> GetActors()
        {
            return _actorRepository.Get();
        }


        public void AddProducer(string name1, string dob1)
        {
            if (string.IsNullOrEmpty(name1) || string.IsNullOrEmpty(dob1))
            {
                throw new ArgumentException("Producer name and dob should not be null");
            }
            //else if (DateTime.Parse(dob).CompareTo(DateTime.Today) > 0)
            //{
            //    throw new ArgumentException("Enter valid DateOfBirth");
            //}
            DateTime dateOfBirth = DateTime.ParseExact(dob1, "yyyy/MM/dd", null);
            if (dateOfBirth > DateTime.Today || dateOfBirth == DateTime.MinValue)
            {
                throw new ArgumentException("Enter valid DateOfBirth");
            }
            var producer = new Person()
            {
                Name = name1,
                DateOfBirth = dateOfBirth
            };

            _producerRepository.Add(producer);
        }
        public List<Person> GetProducer()
        {
            return _producerRepository.Get();
        }





        public List<Person> ChooseActor(string choose)
        {
            //string actorsName = "";
            var actorSelect = choose.Split(' ');
            var actors = new List<Person>();
            var actorPresentList = _actorRepository.Get();
            foreach(var choice in actorSelect)
            {
                int ch = 0;
                try
                {
                    ch = Convert.ToInt32(choice);

                }
                catch (Exception)
                {

                    Console.WriteLine("Actor choice should be an integer") ;
                }
                if (ch < 1 || ch > actorPresentList.Count)
                {
                    Console.WriteLine("Actor choice is not correct");
                }

                actors.Add(actorPresentList[ch - 1]);
            }
            return actors;
           

        }

        public Person ChooseProducer(string choose)
        {
            int chooseProducer = Convert.ToInt32(choose);
            var producerPresentList = _producerRepository.Get();
            if (chooseProducer < 1 || chooseProducer > producerPresentList.Count)
            {
                Console.WriteLine("Producer choice is not correct");
            }
            return producerPresentList[chooseProducer - 1];
        }

    }
}

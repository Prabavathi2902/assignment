﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Domain
{
    public class Person
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Person(string name,DateTime dob)
        {
            Name = name;
            DateOfBirth = dob;
        }

        public Person()
        {
        }
    }
}

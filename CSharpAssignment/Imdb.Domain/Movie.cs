﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Imdb.Domain
{
    public class Movie
    {
        public string Name { get; set; }

        public string Plot { get; set; }

        public int Year { get; set; }

        public Person Producer { get; set; }
        public List<Person> Actors { get; set; }
       
       

       
    }
}

CREATE DATABASE SQLQUERYASSIGNMENT2
USE SQLQUERYASSIGNMENT2

CREATE TABLE Actors(Id int Primary Key Identity(1,1),Name varchar(50),Gender varchar(6),DOB Date);
CREATE TABLE Producers(Id int Primary Key Identity(1,1),Name varchar(50),Company varchar(50),CompanyEstDate Date);
CREATE TABLE Movies(Id int Primary Key Identity(1,1),Name varchar(100),Language varchar(20),ProducerId int FOREIGN KEY REFERENCES Producers(Id),Profit int); 
CREATE TABLE MovieActorRelationship(MovieId int FOREIGN KEY REFERENCES Movies(Id),ActorId int FOREIGN KEY REFERENCES Actors(Id));

ALTER TABLE Movies
ALTER COLUMN Profit decimal



INSERT INTO Actors(Name,Gender,DOB)
VALUES
('Mila Kunis','Female','11/14/1986'),
('Robert DeNiro','Male','07/10/1957'),
('George Michael','Male','11/23/1978'),
('Mike Scott','Male','08/06/1969'),
('Pam Halpert','Female','09/26/1996'),
('Dame Judi Dench','Female','04/05/1947')


SELECT * FROM Actors

INSERT INTO Producers(Name,Company,CompanyEstDate) 
VALUES
   ('Arjun','Fox','05/14/1998'),
   ('Arun','Bull','09/11/2004'),
   ('Tom','Hanks','11/03/1987'),
   ('Zeshan','Male','11/14/1996'),
   ('Nicole','Team Coco','09/26/1992')
SELECT * FROM Producers

INSERT INTO Movies(Name,Language,ProducerId,Profit) 
VALUES
        ('Rocky','English',1,10000),
	    ('Rocky','Hindi',3,3000),
		('Terminal','English',4,300000),
		('Rambo','Hindi',2,93000),
		('Rudy','English',5,9600)
SELECT * FROM Movies

DELETE FROM MovieActorRelationship
INSERT INTO MovieActorRelationship(MovieId,ActorId)
VALUES
(1,1),
(1,3),
(1,5),
(2,6),
(2,5),
(2,4),
(2,2),
(3,3),
(3,2),
(4,1),
(4,6),
(4,3),
(5,2),
(5,5),
(5,3)

SELECT * FROM MovieActorRelationship


--Update Profit of all the movies by +1000 where producer name contains 'run'

UPDATE Movies
SET Profit = Profit+1000
SELECT M.Name,M.Profit
FROM Movies M
JOIN Producers P
ON M.ProducerId=P.Id
Where P.Name LIKE '%run%'

--Find duplicate movies having the same name and their count

SELECT Name,COUNT(*) AS Movies_count
FROM Movies 
GROUP BY Name
HAVING COUNT(*)>1

--Find the oldest actor/actress for each movie

SELECT MIN(DOB), M.Name
FROM Actors A
INNER JOIN MovieActorRelationship MAR ON MAR.ActorId = A.Id
INNER JOIN Movies M ON M.Id = MAR.MovieId
GROUP BY M.Name, M.Id

--List of producers who have not worked with actor X


SELECT * 
FROM Producers P
LEFT JOIN (
SELECT M.ProducerId FROM Movies M
INNER JOIN MovieActorRelationship MAR
ON M.Id=MAR.MovieId
INNER JOIN Actors A
ON A.Id=MAR.ActorId 
WHERE A.Name='Pam Halpert' ) R
ON P.Id=R.ProducerId
WHERE R.ProducerId IS NULL



--List of pair of actors who have worked together in more than 2 movies

SELECT MAR1.ActorId,MAR2.ActorId--,COUNT(*) AS WORKED_Together
FROM MovieActorRelationship MAR1
INNER JOIN MovieActorRelationship MAR2
ON MAR1.MovieId=MAR2.MovieId AND MAR1.ActorId>MAR2.ActorId
GROUP BY MAR1.ActorId,MAR2.ActorId
HAVING COUNT(*)>=1


--Add non-clustered index on profit column of movies table

CREATE NONCLUSTERED INDEX Movies_Profit
ON Movies(Profit);

--Create stored procedure to return list of actors for given movie id

CREATE PROCEDURE ListActors @MovieId int
AS
BEGIN
SELECT A.Name
FROM Actors A
Join MovieActorRelationship MAR
ON MAR.ActorId=A.Id
WHERE MAR.MovieId=@MovieId
END
GO

EXEC ListActors @MovieId=1;

--Create a function to return age for given date of birth

CREATE FUNCTION AgeOfPerson(@DOB DATE)  
RETURNS int   
AS    
BEGIN  
    DECLARE @Age int
    SET @Age=DATEDIFF( YY, @DOB, GETDATE())    
	RETURN @Age
END  


SELECT dbo.AgeOfPerson(DOB)
FROM Actors
--Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated) 

CREATE PROCEDURE ProfitIncrease @MovieIds VARCHAR(50)
AS
UPDATE Movies
SET Profit=Profit+100
WHERE Id IN
(SELECT * 
 FROM STRING_SPLIT(@MovieIds,','))
GO

EXEC ProfitIncrease @MovieIds='1,2,3'
SELECT * FROM Movies





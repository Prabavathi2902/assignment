CREATE DATABASE IMDB
USE IMDB

CREATE TABLE Actors(Id int Primary Key Identity(1,1),Name varchar(200),Gender varchar(6),DOB Date,Biography varchar(1000))
CREATE TABLE Producers(Id int Primary Key Identity(1,1),Name varchar(200),Gender varchar(6),DOB Date,Biography varchar(1000))
CREATE TABLE Movies(Id int Primary Key Identity(1,1),Name varchar(200),YearOfRelease int,Plot varchar(20),Poster VarBinary(MAX),ProducerId int FOREIGN KEY REFERENCES Producers(Id))
CREATE TABLE MovieActorMapping(MovieId int FOREIGN KEY REFERENCES Movies(Id),ActorId int FOREIGN KEY REFERENCES Actors(Id))
CREATE TABLE Genres(Id int Primary Key Identity(1,1),Name varchar(100))
CREATE TABLE MovieGenreMapping(MovieId int FOREIGN KEY REFERENCES Movies(Id),GenreId int FOREIGN KEY REFERENCES Genres(Id))

CREATE DATABASE SCHOOLDB
USE SCHOOLDB
CREATE TABLE Classes(Id int Primary Key Identity(1,1),Name varchar(50),Section varchar(2),Number int);
CREATE TABLE Teachers(Id int Primary Key Identity(1,1),Name varchar(50),DOB DATE,Gender varchar(6));
CREATE TABLE Students(Id int Primary Key Identity(1,1),Name varchar(50),DOB DATE,Gender varchar(6),ClassId int);
CREATE TABLE TeacherClassMapping(TeacherId int,ClassId int)


ALTER TABLE Students
ADD FOREIGN KEY (ClassId)
REFERENCES Classes(Id)

ALTER TABLE TeacherClassMapping
ADD FOREIGN KEY (TeacherId)
REFERENCES Teachers(Id)

ALTER TABLE TeacherClassMapping
ADD FOREIGN KEY (ClassId)
REFERENCES Classes(Id)


INSERT INTO Classes (Name,Section,Number)
VALUES
('IX','A',201),
('IX','B',202),
('X','A',203)

INSERT INTO Teachers(Name,DOB,Gender)
VALUES 
('Lisa Kudrow','1985/06/08','Female'),
('Monica Bing','1982/03/06','Female'),
('Chandler Bing','1978/12/17','Male'),
('Ross Geller','1993/01/26','Male')

INSERT INTO Students(Name,DOB,Gender,ClassId) 
VALUES
('Scotty Loman','2006/01/31','Male',1),
('Adam Scott','2005/06/01','Male',1),
('Natosha Beckles','2005/01/23','Female',2),
('Lilly Page','2006/11/26','Female',2),
('John Freeman','2006/06/14','Male',2),
('Morgan Scott','2005/05/18','Male',3),
('Codi Gass','2005/12/24','Female',3),
('Nick Roll','2005/12/24','Male',3),
('Dave Grohl','2005/02/12','Male',3)

INSERT INTO TeacherClassMapping(TeacherId,ClassId) 
values
(1,1),
(1,2),
(2,2),
(2,3),
(3,3),
(3,1)

SELECT * FROM Students
SELECT * FROM Teachers
SELECT * FROM Classes
SELECT * FROM TeacherClassMapping


--Find list of male students 

SELECT * 
FROM Students
WHERE Gender='Male'

--Find list of student older than 2005/01/01
SELECT * 
From Students
WHERE DOB >'2005/01/01'

--Youngest student in school

SELECT Name AS Youngest_Student
FROM Students
WHERE dob=(SELECT MAX(DOB) 
FROM Students)

--Find student distinct birthdays

SELECT DISTINCT(DOB)
FROM Students

-- No of students in each class

SELECT C.Name,COUNT(*) AS NO_OF_STUDENTS
FROM Students S
JOIN Classes C
ON S.ClassId=C.Id
GROUP BY C.Name

-- No of students in each section

SELECT C.Name,C.Section,COUNT(*) AS Count
FROM Students S
JOIN Classes C
ON S.ClassId=C.Id
GROUP BY C.Name,C.Section

-- No of classes taught by teacher

SELECT T.Id,T.Name,COUNT(*)
FROM Teachers T
JOIN TeacherClassMapping TCM
ON T.Id=TCM.TeacherId
GROUP BY T.Id,T.Name
-- List of teachers teaching Class X

SELECT T.Name
FROM Teachers T
JOIN TeacherClassMapping TCM
ON T.Id=TCM.TeacherId
JOIN Classes C
ON TCM.ClassId=C.Id
AND C.Name='X'
-- Classes which have more than 1 teachers teaching

SELECT C.Name,C.Section,COUNT(TCM.TeacherId) AS CLASSCOUNT
FROM Classes C
JOIN TeacherClassMapping TCM
ON C.Id=TCM.ClassId 
GROUP BY C.Name,C.Section
HAVING COUNT(TCM.TeacherId)>1


-- List of students being taught by 'Lisa'

SELECT S.Name
FROM Teachers T
JOIN TeacherClassMapping TCM
ON T.Id=TCM.TeacherId 
JOIN Students S
ON S.ClassId=TCM.ClassId
AND T.Name='Lisa Kudrow' 




